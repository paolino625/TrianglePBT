package it.unimi.di.vec.ass1;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

import java.io.*;
import net.jqwik.api.*;

public class TrianglePBT_testWithGenerators {

  @Property(seed = "-42", tries = 50000)
  void testTriangoliScaleni(@ForAll("ArrayTreLati0Uguali") Integer[] sides) {
    Assume.that(areValidSides(sides));
    // Statistics.collect(areValidSides(sides) ? "valid" : "invalid"); // È possibile tenere traccia
    // di quanti casi sono triangoli validi e quanti no (overflow)
    assertThat(triangleDescribeAsAFunction(sides)).isEqualToIgnoringNewLines("scaleno");
  }

  @Property(seed = "-42", tries = 50000)
  void testTriangoliIsosceli(@ForAll("ArrayTreLati2Uguali") Integer[] sides) {
    assertThat(triangleDescribeAsAFunction(sides)).isEqualToIgnoringNewLines("isoscele");
  }

  @Property(seed = "-42", tries = 50000)
  void testTriangoliEquilateri(@ForAll("ArrayTreLati3Uguali") Integer[] sides) {
    assertThat(triangleDescribeAsAFunction(sides)).isEqualToIgnoringNewLines("equilatero");
  }

  public static boolean areValidSides(Integer[] sides) {
    // Triangoli validi per un triangolo: utilizzo di long per evitare overflow
    return (((long) sides[0] + (long) sides[1]) > sides[2])
        && (((long) sides[0] + (long) sides[2]) > sides[1])
        && (((long) sides[1] + (long) sides[2]) > sides[0]);
  }

  public String triangleDescribeAsAFunction(Integer[] sides) {

    String input = sides[0] + " " + sides[1] + " " + sides[2];
    InputStream in = new ByteArrayInputStream(input.getBytes());
    OutputStream out = new ByteArrayOutputStream();

    MyTriangle t = new MyTriangle(in, new PrintStream(out));
    t.describe();

    return out.toString();
  }

  @Provide
  Arbitrary<Integer[]> ArrayTreLati0Uguali() {
    Arbitrary<Integer> ints = Arbitraries.integers().between(1, Integer.MAX_VALUE);
    return ints.array(Integer[].class)
        .uniqueElements()
        .ofSize(3)
        .filter(TrianglePBT_testWithGenerators::areValidSides);
  }

  // Sarebbe meglio se ArrayTreLati2Uguali restituisse triangoli "equilibrati", ovvero:
  // - Il 33 % delle volte i primi due lati uguali.
  // - Il 33 % delle volte il primo e il terzo lato uguali.
  // - Il 33 % delle volte il secondo e il terzo lato uguali.

  @Provide
  Arbitrary<Integer[]> ArrayTreLati2Uguali() {
    Arbitrary<Integer> a = Arbitraries.integers().between(1, Integer.MAX_VALUE);
    return a.flatMap(
        x ->
            Arbitraries.integers()
                .between(1, Integer.MAX_VALUE)
                .filter(z -> !z.equals(x))
                .map(y -> new Integer[] {x, x, y}));
  }

  @Provide
  Arbitrary<Integer[]> ArrayTreLati3Uguali() {
    Arbitrary<Integer> a = Arbitraries.integers().between(1, Integer.MAX_VALUE);
    return a.flatMap(x -> Arbitraries.integers().map(y -> new Integer[] {x, x, x}));
  }

  @Provide
  Arbitrary<Integer[]> ArrayTreLatiCasuali() {

    return Arbitraries.oneOf(ArrayTreLati0Uguali(), ArrayTreLati2Uguali(), ArrayTreLati3Uguali());
  }
}
