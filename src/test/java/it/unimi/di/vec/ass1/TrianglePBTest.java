package it.unimi.di.vec.ass1;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.*;
import java.util.Random;
import net.jqwik.api.Example;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.Positive;

public class TrianglePBTest {

  @Example
  void testEquilatero() {
    // SETUP
    InputStream in = new ByteArrayInputStream("3 3 3".getBytes());
    OutputStream out = new ByteArrayOutputStream();

    // EXERCISE
    MyTriangle t = new MyTriangle(in, new PrintStream(out));
    t.describe();

    // VERIFY
    assertThat(out.toString()).isEqualToIgnoringNewLines("equilatero");
  }

  @Property(tries = 100, seed = "424242")
  void testTriangoliEquilateri(@ForAll @Positive int x) {

    // SETUP

    String numeroRandom = Integer.toString(x);
    String stringaInput = numeroRandom + " " + numeroRandom + " " + numeroRandom;

    InputStream in = new ByteArrayInputStream(stringaInput.getBytes());
    OutputStream out = new ByteArrayOutputStream();

    // EXERCISE
    MyTriangle t = new MyTriangle(in, new PrintStream(out));
    t.describe();

    // VERIFY
    assertThat(out.toString()).isEqualToIgnoringNewLines("equilatero");
  }

  @Property(tries = 100, seed = "424242")
  void testTriangoliScaleni(@ForAll @Positive long x) {

    // SETUP

    String stringaInput = generaLatiTriangoloScalenoValido(x);

    InputStream in = new ByteArrayInputStream(stringaInput.getBytes());
    OutputStream out = new ByteArrayOutputStream();

    // EXERCISE
    MyTriangle t = new MyTriangle(in, new PrintStream(out));
    t.describe();

    // VERIFY
    assertThat(out.toString()).isEqualToIgnoringNewLines("scaleno");
  }

  private String generaLatiTriangoloScalenoValido(long seed) {

    Random random = new Random(seed);
    int x, y, z;

    do {
      x = random.nextInt() & Integer.MAX_VALUE;
      y = random.nextInt() & Integer.MAX_VALUE;
      z = random.nextInt() & Integer.MAX_VALUE;
    } while ((x + y > z) && (y + z > x) && (x + z > y));

    return x + " " + y + " " + z;
  }
}
