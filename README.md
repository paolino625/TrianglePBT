**INDICE**

| Assignment | Argomento Lezione | Task Svolti |
| ---------- | --------- | ------------------- |
| [Assignment9b](https://gitlab.com/paolino625/TrianglePBT/-/tree/calcagni_paolo_assignment9b) | Introduzione PBT. | Applicato PBT a Triangle sia per i triangoli equilateri che scaleni.|
| [Assignment11](https://gitlab.com/paolino625/TrianglePBT/-/tree/calcagni_paolo_assignment11) | Generatori Jqwik. | Creati generatori per triangoli equilateri, isosceli e scaleni. Creato generatore casuale di triangoli. |

**CONSIGLIO CLONAZIONE REPO**

Attenzione contiene un sottomodulo con la versione di `triangles` con la *dependency injection*.

Clonare con

```sh
git clone --recurse-submodules
```
